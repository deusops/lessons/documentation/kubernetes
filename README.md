# Базовые функции и утилиты
- [Шпаргалка по kubectl](https://kubernetes.io/ru/docs/reference/kubectl/cheatsheet/)
- [Установка kubectl](https://kubernetes.io/ru/docs/tasks/tools/install-kubectl/)
- [Автодополнение для shell](https://kubernetes.io/ru/docs/reference/kubectl/cheatsheet/#kubectl-autocomplete)
- [K9S: Консольная утилита для удобной работы](https://habr.com/ru/companies/flant/articles/524196/)

# Minikube
- [Установка minukube](https://minikube.sigs.k8s.io/docs/start/)
- [Альтернатива в виде kind](https://kind.sigs.k8s.io/)

# Яндекс Облако
- [Установка Yandex Cloud CLI](https://cloud.yandex.ru/ru/docs/cli/operations/install-cli)
- [Получение kubeconfig](https://cloud.yandex.ru/ru/docs/cli/cli-ref/managed-services/managed-kubernetes/cluster/get-credentials)

# Persistent Volumes

- [NFS в качестве PersistentVolume](https://www.digitalocean.com/community/tutorials/how-to-set-up-readwritemany-rwx-persistent-volumes-with-nfs-on-digitalocean-kubernetes-ru)
- [Kubernetes: StorageClasses](https://kubernetes.io/docs/concepts/storage/storage-classes/)
- Kubernetes: PV и PVC
    - https://kubernetes.io/docs/concepts/storage/persistent-volumes/
    - https://rtfm.co.ua/kubernetes-persistentvolume-i-persistentvolumeclaim-obzor-i-primery/

# Автоскейлинг
- [Автоскейлинг приложений Kubernetes](https://habr.com/ru/company/vk/blog/515114/)
- [Автомасштабирование Kubernetes на Yandex Cloud](https://cloud.yandex.ru/docs/managed-kubernetes/concepts/node-group/cluster-autoscaler)